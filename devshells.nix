{ inputs, ... }:
{
  perSystem =
    {
      pkgs,
      system,
      config,
      self',
      ...
    }:
    {
      devShells = {
        default = pkgs.mkShellNoCC {
          packages = [
            pkgs.colmena
            pkgs.age
            pkgs.sops
            pkgs.nil
            self'.packages.repl
          ];

          inputsFrom = [
            config.flake-root.devShell
            config.treefmt.build.devShell
            config.pre-commit.devShell
          ];

          shellHook = ''
            ln -sf ${inputs.self.packages.${system}.sopsYaml} "$FLAKE_ROOT/.sops.yaml"
          '';
        };

        ci = pkgs.mkShellNoCC {
          inputsFrom = [ self'.devShells.default ];
          packages = [
            pkgs.attic-client
            self'.packages.nix-fast-build
          ];
        };
      };
    };
}
