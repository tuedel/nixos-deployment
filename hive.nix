{
  inputs,
  system ? "x86_64-linux",
}:
{
  meta = {
    nixpkgs = import inputs.nixpkgs { inherit system; };
    specialArgs = {
      inherit inputs;
    };
  };

  defaults =
    {
      config,
      lib,
      name,
      ...
    }:
    {
      deployment = {
        targetHost = lib.mkDefault "${name}.club.muc.ccc.de";
        targetUser = lib.mkDefault null;
        tags = [ config.nixpkgs.system ];
      };
      imports = [
        inputs.self.outputs.nixosModules.all
        "${inputs.self}/hosts/${name}.nix"
      ];
      muccc.targetDeploy = true;
    };

  briafzentrum = {
    deployment.targetHost = "briafzentrum.muc.ccc.de";
    deployment.tags = [ "infra" ];
  };

  nixbus = {
    deployment.tags = [ "infra" ];
  };

  loungepi = {
    deployment.tags = [ "infra" ];
  };

  hauptraumpi = {
    deployment.tags = [ "infra" ];
  };

  luftschleuse = {
    deployment.tags = [ "infra" ];
  };

  prometheus = {
    deployment.tags = [ "infra" ];
  };

  brezn = {
    deployment.targetHost = "brezn.muc.ccc.de";
    deployment.tags = [
      "dns"
      "infra"
    ];
  };

  zonk = {
    deployment.targetHost = "zonk.muc.ccc.de";
    deployment.tags = [
      "dns"
      "infra"
    ];
  };

  # zock = {
  #   deployment.tags = [ "infra" ];
  # };

  nginx = {
    deployment.tags = [ "infra" ];
  };

  auth = {
    deployment.tags = [ "infra" ];
  };

  gitlab-runner = {
    deployment.tags = [ "infra" ];
  };

  nextcloud = {
    deployment.tags = [ "infra" ];
  };

  netbox = {
    deployment.tags = [ "infra" ];
  };

  pad = {
    deployment.tags = [ "infra" ];
  };

  gitea = {
    deployment.tags = [ "infra" ];
  };

  oaarchkatzl = {
    deployment.targetHost = "oaarchkatzl.muc.ccc.de";
    deployment.tags = [ "infra" ];
  };

  drugga = { };

  laborbox = { };
}
