{ config, ... }:
let
  inherit (builtins) toString;
in
{
  system.stateVersion = "23.05";

  sops = {
    defaultSopsFile = ../secrets/oaarchkatzl.yaml;
    secrets.coturn_auth_secret = {
      owner = "turnserver";
    };

    secrets.prosody_turn_secret = {
      owner = "prosody";
    };
  };

  muccc.hcloud.aarch64.enable = config.muccc.targetDeploy;

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/730A-DC91";
    fsType = "vfat";
  };

  networking = {
    hostName = "oaarchkatzl";
    domain = "muc.ccc.de";

    firewall.allowedTCPPorts = [
      config.services.murmur.port
      config.services.coturn.listening-port
      config.services.coturn.tls-listening-port
      config.services.coturn.alt-listening-port
      config.services.coturn.alt-tls-listening-port
    ];
    firewall.allowedUDPPorts = [
      config.services.murmur.port
      config.services.coturn.listening-port
      config.services.coturn.tls-listening-port
      config.services.coturn.alt-listening-port
      config.services.coturn.alt-tls-listening-port
    ];
    firewall.allowedUDPPortRanges = [
      {
        from = config.services.coturn.min-port;
        to = config.services.coturn.max-port;
      }
    ];
  };

  services.cloud-init.enable = false;
  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "96:00:02:6b:e1:52";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    address = [
      "5.75.248.200"
      "2a01:4f8:c012:1a6a::/64"
    ];
    gateway = [ "fe80::1" ];
    routes = [
      {
        routeConfig = {
          Gateway = "172.31.1.1";
          GatewayOnLink = true;
        };
      }
    ];
    dns = [ "8.8.8.8" ];
  };

  services.murmur = {
    enable = true;
    registerName = "Club Mumble";
    allowHtml = true;
    logDays = -1;
    logFile = null;
    password = "";
    registerHostname = "fasel.muc.ccc.de";
    welcometext = "Willkommen im µc³ Mumble.";
    sslCert = "/var/lib/acme/fasel.muc.ccc.de/fullchain.pem";
    sslKey = "/var/lib/acme/fasel.muc.ccc.de/key.pem";
    bandwidth = 130000;
    extraConfig = ''
      ice="tcp -h 127.0.0.1 -p 6502"
      autobanAttempts=0
      obfuscate=true
      opusthreshold=0
      suggestPushToTalk=true
    '';
  };

  services.jitsi-meet = {
    enable = true;
    hostName = "webex.muc.ccc.de";
    nginx.enable = true;

    config = {
      startWithVideoMuted = true;
      desktopSharingFrameRate = {
        min = 5;
        max = 33;
      };
      channelLastN = 11;
      prejoinConfig = {
        enabled = true;
      };
      enableWebHIDFeature = true;
      disableThirdPartyRequests = true;
      analytics = {
        enabled = false;
      };
      liveStreamingEnabled = false;
      fileRecordingsEnabled = false;
      useStunTurn = true;
      p2p = {
        useStunTurn = true;
        stunServers = [
          { urls = "stun:${config.services.coturn.realm}:${toString config.services.coturn.listening-port}"; }
        ];
      };
    };

    interfaceConfig = {
      SHOW_JITSI_WATERMARK = false;
    };
  };

  services.jitsi-videobridge = {
    openFirewall = true;
  };

  services.prosody = {
    # FIXME: TURN currently doesn't work because prosody does not provide any TURN servers
    extraModules = [ "turncredentials" ];
    extraConfig = ''
      turncredentials_secret = os.getenv("PROSODY_TURN_SECRET")
      turncredentials_host = "${config.services.coturn.realm}"

      log = { warn = "*syslog"; }
    '';
  };

  systemd.services.prosody.serviceConfig.EnvironmentFile = [
    config.sops.secrets.prosody_turn_secret.path
  ];
  systemd.services.jicofo.after = [ "prosody.service" ];

  services.coturn = {
    enable = true;
    cert = "/var/lib/acme/webex.muc.ccc.de/fullchain.pem";
    pkey = "/var/lib/acme/webex.muc.ccc.de/key.pem";
    realm = config.services.jitsi-meet.hostName;
    use-auth-secret = true;
    static-auth-secret-file = config.sops.secrets.coturn_auth_secret.path;
  };

  users.extraGroups.nginx.members = [
    "murmur"
    "turnserver"
  ];

  security.acme.certs = {
    "fasel.muc.ccc.de" = {
      postRun = ''
        systemctl restart murmur
      '';
    };
  };

  services.nginx.enable = true;
  services.nginx = {
    virtualHosts."oaarchkatzl.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      default = true;
      locations."/".return = "200 ''";
    };

    virtualHosts."fasel.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/".return = "200 ''";
    };
  };
}
