{
  config,
  pkgs,
  lib,
  ...
}:
{
  system.stateVersion = "23.05";

  sops = {
    defaultSopsFile = ../secrets/briafzentrum.yaml;
    secrets.dovecot_passdb = {
      owner = "dovecot2";
    };
    secrets.virtuals_map = { };
    secrets.srs_secrets = { };
  };

  hardware.enableAllFirmware = lib.mkForce false;

  networking = {
    hostName = "briafzentrum";
    domain = "muc.ccc.de";
    firewall.allowedTCPPorts = [ 25 ];
  };

  systemd.network.links."30-upl1nk" = {
    matchConfig.PermanentMACAddress = "86:51:21:03:3d:37";
    linkConfig.Name = "upl1nk";
  };

  systemd.network.networks."30-upl1nk" = {
    matchConfig.Name = "upl1nk";
    networkConfig = {
      Address = [
        "2001:1578:400:23:feed:fefe:23:42/64"
        "194.126.158.122/29"
      ];
      Gateway = [
        "2001:1578:400:23::1"
        "194.126.158.121"
      ];
      DNS = [
        "::1"
        "127.0.0.1"
      ];
      IPv6AcceptRA = false;
    };
  };

  environment.systemPackages = with pkgs; [ mailutils ];

  services.kresd = {
    enable = true;
    listenPlain = [
      "127.0.0.1:53"
      "[::1]:53"
    ];
  };

  services.redis.servers."".enable = true;

  services.nginx = {
    enable = true;

    serverNamesHashBucketSize = 512;

    virtualHosts."briafzentrum.muc.ccc.de" = {
      default = true;

      enableACME = true;
      forceSSL = true;
      basicAuth.briaftraeger = "briafgeheimnis";
      locations = {
        "/".return = "200 ''";
        "/rspamd/" = {
          proxyPass = "http://[::1]:11334/";
          recommendedProxySettings = false;
          extraConfig = ''
            proxy_set_header X-Forwarded-For "";
            proxy_set_header X-Real-IP "";
          '';
        };
      };
    };

    virtualHosts."lists.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
    };
  };

  users.extraGroups.acme.members = [ "postfix" ];

  services.pfix-srsd.domain = "muc.ccc.de";
  services.pfix-srsd.secretsFile = config.sops.secrets.srs_secrets.path;

  services.postfix = {
    enable = true;
    hostname = "briafzentrum.muc.ccc.de";
    destination = [
      "briafzentrum.muc.ccc.de"
      "muc.ccc.de"
      "muenchen.ccc.de"
      "badge.events.ccc.de"
      "r0ket.badge.events.ccc.de"
      "rad1o.badge.events.ccc.de"
    ];
    relayDomains = [ "hash:/var/lib/mailman/data/postfix_domains" ];
    recipientDelimiter = "+";
    networks = [
      "[::1]"
      "127.0.0.1"
      "194.126.158.123"
      "83.133.178.71/32"
      "[2a0e:8f04:1001:b2::13]/128"
    ];
    postmasterAlias = "noc@muc.ccc.de";
    extraAliases = ''
      hostmaster: postmaster
      abuse: postmaster
      mailman: codec+bmm@fnord.cx
    '';
    transport = ''
      briafzentrum.muc.ccc.de local:
    '';
    mapFiles = {
      sender_login_map = ../static/briafzentrum/sender_login_map;
      virtuals_map = config.sops.secrets.virtuals_map.path;
      blacklist_map = ../static/briafzentrum/blacklist_map;
    };
    useSrs = true;
    config = {
      biff = "no";

      append_dot_mydomain = "no";
      delay_warning_time = "4h";
      mailbox_size_limit = "0";
      transport_maps = [ "hash:/var/lib/mailman/data/postfix_lmtp" ];
      virtual_alias_maps = [ "hash:/var/lib/postfix/conf/virtuals_map" ];
      local_recipient_maps = [
        "proxy:unix:passwd.byname $alias_maps"
        "hash:/var/lib/mailman/data/postfix_lmtp"
      ];

      smtpd_sasl_type = "dovecot";
      smtpd_sasl_path = "/run/dovecot2/auth";
      smtpd_sasl_auth_enable = "yes";
      smtpd_sasl_security_options = "noanonymous";
      smtpd_sender_login_maps = [ "hash:/var/lib/postfix/conf/sender_login_map" ];

      smtpd_recipient_restrictions = [
        "check_client_access cidr:/var/lib/postfix/conf/blacklist_map"
        "permit_mynetworks"
        "permit_sasl_authenticated"
        "permit_auth_destination"
        "reject_unauth_destination"
        "reject_unknown_recipient_domain"
        "reject_unverified_recipient"
      ];

      smtpd_relay_restrictions = [
        "permit_mynetworks"
        "permit_sasl_authenticated"
        "permit_auth_destination"
        "reject_unauth_destination"
        "reject_unknown_recipient_domain"
        "reject_unverified_recipient"
      ];

      smtpd_sender_restrictions = [ "reject_authenticated_sender_login_mismatch" ];
      smtpd_helo_required = "yes";
      disable_vrfy_command = "yes";
      unverified_recipient_reject_reason = "Address lookup failed";
      smtpd_discard_ehlo_keywords = [
        "silent-discard"
        "dsn"
      ];

      smtpd_tls_auth_only = "yes";
      smtpd_tls_cert_file = "/var/lib/acme/briafzentrum.muc.ccc.de/fullchain.pem";
      smtpd_tls_key_file = "/var/lib/acme/briafzentrum.muc.ccc.de/key.pem";
      smtpd_tls_loglevel = "1";
      smtpd_tls_received_header = "yes";
      smtpd_tls_security_level = "may";
      smtpd_tls_session_cache_database = "btree:/var/lib/postfix/data/smtpd_scache";
      smtpd_tls_session_cache_timeout = "3600s";
      smtpd_tls_protocols = [
        "!SSLv2"
        "!SSLv3"
      ];
      smtpd_tls_ciphers = "high";
      smtpd_tls_exclude_ciphers = [
        "aNULL"
        "MD5"
        "RC4"
        "3DES"
        "DES"
        "PSK"
        "DSS"
        "SRP"
        "ADH"
        "AECDH"
      ];
      smtpd_tls_CAfile = "/etc/ssl/certs/ca-certificates.crt";

      tls_ssl_options = [
        "no_ticket"
        "no_compression"
      ];
      tls_preempt_cipherlist = "yes";
      tls_append_default_CA = "yes";

      smtp_tls_security_level = "may";
      smtp_tls_loglevel = "1";
      smtp_tls_protocols = [
        "!SSLv2"
        "!SSLv3"
      ];
      smtp_tls_ciphers = "high";
      smtp_tls_exclude_ciphers = [
        "aNULL"
        "MD5"
        "RC4"
        "3DES"
        "DES"
        "PSK"
        "DSS"
        "SRP"
        "ADH"
        "AECDH"
      ];
      smtp_address_preference = "ipv6";

      smtpd_milters = [ "unix:/run/rspamd/rspamd.sock" ];
      milter_protocol = "6";
      milter_mail_macros = "i {mail_addr} {client_addr} {client_name} {auth_authen}";
      milter_default_action = "tempfail";
    };
  };

  services.dovecot2 = {
    enable = true;
    enablePAM = false;
    enableImap = false;
    enableLmtp = false;

    extraConfig = ''
      passdb {
        driver = passwd-file
        args = "${config.sops.secrets.dovecot_passdb.path}"
      }

      service auth {
        unix_listener auth {
          mode = 0660
          user = "dovecot2"
          group = "postfix"
        }
      }

      auth_mechanisms = plain login
    '';
  };

  services.prometheus.exporters.postfix = {
    enable = true;
    openFirewall = true;
  };

  users.users.postfix-exporter.extraGroups = [ "systemd-journal" ];

  nixpkgs.overlays = [
    (_self: super: {
      mailmanPackages = super.mailmanPackages.extend (
        _mmself: mmsuper: {
          python3 = mmsuper.python3.override (_: {
            overlay = (
              pyself: pysuper: {
                readme-renderer = pysuper.readme-renderer.overridePythonAttrs (_: {
                  propagatedBuildInputs = [ pyself.cmarkgfm ];
                });
              }
            );
          });
        }
      );
    })
  ];

  services.mailman = {
    enable = true;
    siteOwner = "mailman@briafzentrum.muc.ccc.de";
    webHosts = [ "lists.muc.ccc.de" ];
    serve.enable = true;
    serve.virtualRoot = "/";
    hyperkitty.enable = true;

    webSettings = {
      POSTORIUS_TEMPLATE_BASE_URL = "http://127.0.0.1:18507";
    };
  };

  systemd.services.mailman-uwsgi.environment = {
    "UWSGI_MASTER" = "1";
    "UWSGI_WORKERS" = "4";
    "UWSGI_THREADS" = "2";
  };

  services.rspamd = {
    enable = true;

    workers.controller.bindSockets = [
      "[::1]:11334"
      {
        socket = "/run/rspamd/rspamd-worker.sock";
        mode = "0666";
        owner = "rspamd";
        group = "rspamd";
      }
    ];

    workers.rspamd_proxy = {
      bindSockets = [
        {
          socket = "/run/rspamd/rspamd.sock";
          mode = "0666";
          owner = "rspamd";
          group = "postfix";
        }
      ];
      extraConfig = ''
        upstream "local" {
          self_scan = yes;
        }
      '';
    };

    overrides."options.inc".text = ''
      local_addrs = [127.0.0.1, ::1, 194.126.158.124, 83.133.178.160/28];
    '';

    locals."milter_headers.conf".text = ''
      extended_spam_headers = true;
    '';

    locals."settings.conf".text = ''
      local {
        local = yes;
        apply {
          groups_disabled = ["rbl", "spf", "headers", "statistics"];
        }
      }
      authenticated {
        priority = high;
        authenticated = yes;
        apply {
          groups_disabled = ["rbl", "spf"];
        }
      }
    '';

    locals."redis.conf".text = ''
      servers = "127.0.0.1";
      db = 1;
    '';

    locals."reputation.conf".text = ''
      rules {
        ip_reputation = {
          selector "ip" {
          }
          backend "redis" {
            servers = "127.0.0.1";
          }

          symbol = "IP_REPUTATION";
        }
      }
    '';

    locals."groups.conf".text = ''
      group "reputation" {
        symbols = {
          "IP_REPUTATION_HAM" {
              weight = 1.0;
          }
          "IP_REPUTATION_SPAM" {
              weight = 4.0;
          }
        }
      }
    '';

    locals."mx_check.conf".text = ''
      enabled = yes;
    '';

    locals."history_redis.conf".text = ''
      nrows = 1000;
    '';

    locals."classifier_bayes.conf".text = ''
      autolearn = [-4, 12];
    '';
  };
}
