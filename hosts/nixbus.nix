{
  lib,
  config,
  pkgs,
  ...
}:
{
  system.stateVersion = "23.05";

  sops = {
    defaultSopsFile = ../secrets/nixbus.yaml;
    secrets.schleuse_irc_env = { };
    secrets.api_env = { };
  };

  boot = {
    supportedFilesystems = [ "xfs" ];
  };

  fileSystems = {
    "/" = lib.mkIf config.muccc.targetDeploy {
      device = "/dev/vda2";
      fsType = lib.mkForce "xfs";
      options = [
        "noatime"
        "nodiratime"
        "logbufs=8"
      ];
    };
  };

  swapDevices = [ { device = "/dev/vda3"; } ];

  networking = {
    hostName = "nixbus";
    domain = "club.muc.ccc.de";
    firewall = {
      allowedTCPPorts = [ 22 ];
      allowedUDPPorts = [
        2080 # schleusenstate multicast
      ];
    };
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "00:20:91:f9:b9:f9";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [ "83.133.178.67/26" ];
      Gateway = [ "83.133.178.65" ];
      DNS = [ "83.133.178.65" ];
    };
  };

  # MuCCC API
  services.nginx = {
    enable = true;
    virtualHosts."nixbus.club.muc.ccc.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".return = "204";
    };
    virtualHosts."api.muc.ccc.de" = {
      enableACME = true;
      addSSL = true;
      locations."/" = {
        proxyPass = "http://[::1]:8020";
        proxyWebsockets = true;
      };
      locations."/wiki_kalender.ics".return = "301 https://api.muc.ccc.de/events/all.ics";
      locations."/wiki_kalender_public.ics".return = "301 https://api.muc.ccc.de/events/public.ics";
    };
  };

  systemd.services.muccc-api = {
    wantedBy = [ "multi-user.target" ];
    description = "MuCCC API";
    serviceConfig = {
      ExecStart = "${pkgs.muccc-api}/bin/muccc-api";
      EnvironmentFile = [ config.sops.secrets.api_env.path ];
      Restart = "always";
      DynamicUser = true;
      PrivateTmp = true;
    };
  };

  systemd.services.muccc-irc-schleuse = {
    wantedBy = [ "multi-user.target" ];
    description = "MuCCC IRC bot schleuse";
    serviceConfig = {
      ExecStart = "${pkgs.muccc-api}/bin/muccc-irc-schleuse";
      EnvironmentFile = [ config.sops.secrets.schleuse_irc_env.path ];
      Restart = "always";
      DynamicUser = true;
      PrivateTmp = true;
    };
  };

  systemd.services.muccc-flipdots = {
    wantedBy = [ "multi-user.target" ];
    description = "MuCCC Flipdots Update";
    serviceConfig = {
      ExecStart = "${pkgs.muccc-api}/bin/muccc-flipdots";
      Restart = "always";
      DynamicUser = true;
      PrivateTmp = true;
    };
  };
}
