{
  config,
  lib,
  nodes,
  ...
}:
let
  genPrometheusTargets =
    filterfn: targetfn:
    let
      hosts = lib.filter filterfn (lib.attrValues nodes);
    in
    map targetfn hosts;

  checkAll = checks: node: lib.all (f: f node.config) checks;
  exporterCheck = exporterName: config: config.services.prometheus.exporters.${exporterName}.enable;
  infraCheck = config: lib.elem "infra" config.deployment.tags;

  nodeExporterTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "node")
      ])
      (
        c: "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.node.port}"
      )
    # manual targets
    ++ [
      "gustl.club.muc.ccc.de:9100"
      "space.club.muc.ccc.de:9100"
      "xaver.muc.ccc.de:9100"
      "ludwig2.muc.ccc.de:9100"
      "bumbl.muc.ccc.de:9100"
    ];

  postgresTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "postgres")
      ])
      (
        c:
        "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.postgres.port}"
      );

  knotTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "knot")
      ])
      (
        c: "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.knot.port}"
      );

  postfixTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "postfix")
      ])
      (
        c:
        "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.postfix.port}"
      );

  nextcloudTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "nextcloud")
      ])
      (
        c:
        "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.nextcloud.port}"
      );

  redisTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "redis")
      ])
      (
        c: "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.redis.port}"
      );

  giteaTargets = genPrometheusTargets (c: c.config.services.gitea.enable) (
    c: "${c.config.services.gitea.settings.server.DOMAIN}"
  );

  nginxTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "nginx")
      ])
      (
        c: "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.nginx.port}"
      )
    # manual targets
    ++ [ "ludwig2.muc.ccc.de:9113" ];

  httpNginxVhosts =
    lib.filter
      (
        v:
        builtins.elem v [
          # only redirect
          "pads.muc.ccc.de"
        ]
      )
      (
        lib.flatten (
          map (c: lib.mapAttrsToList (n: _: n) c.config.services.nginx.virtualHosts) (
            lib.filter (c: c.config.services.nginx.enable) (lib.attrValues nodes)
          )
        )
        # manual targets
        ++ [
          "muc.ccc.de"
          "rad1o.badge.events.ccc.de"
          "otrs.muc.ccc.de"
        ]
      );

  /*
    snmpTargets = [
      "83.133.178.62" # Core / cs2960
      "83.133.178.61" # Environment / ds2510g48
      "83.133.178.60" # PoE ds2960
      # access points
      "83.133.178.52"
      "83.133.178.53"
      "83.133.178.54"
      "83.133.178.55"
      "83.133.178.56"
      "83.133.178.57"
      # garching / mikrotik router
      "192.168.178.1"
    ];
  */

  # common relabel configs, append or apply in jobs as needed
  relabelHostnameFromInstance = {
    source_labels = [ "__address__" ];
    regex = "([^:]+):\\d+";
    target_label = "hostname";
  };
  commonRelabelConfigs = [ relabelHostnameFromInstance ];
in
{
  system.stateVersion = "23.05";

  sops = {
    defaultSopsFile = ../secrets/prometheus.yaml;
    secrets.pve_env = { };
    secrets.grafana-oidc-client = {
      owner = "grafana";
    };
    secrets.grafana-oidc-secret = {
      owner = "grafana";
    };
  };

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-uuid/EFA8-248B";
      fsType = "vfat";
    };
  };

  swapDevices = [ { device = "/dev/disk/by-uuid/a76d7dc8-01c2-411a-909c-b5713142e56c"; } ];

  networking = {
    hostName = "prometheus";
    domain = "club.muc.ccc.de";
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "5a:66:9d:22:e5:44";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [ "83.133.178.72/26" ];
      Gateway = [ "83.133.178.65" ];
      DNS = [
        "::1"
        "127.0.0.1"
      ];
    };
  };

  services.kresd = {
    enable = true;
    listenPlain = [
      "127.0.0.1:53"
      "[::1]:53"
    ];
  };

  services.prometheus = {
    enable = true;
    enableReload = true;
    extraFlags = [
      "--query.max-concurrency=100"
      "--web.max-connections=512"
      "--storage.tsdb.retention.time=90d"
      "--storage.tsdb.wal-compression"
    ];

    webExternalUrl = "https://prometheus.club.muc.ccc.de/";

    exporters.pve.enable = true;
    exporters.pve.environmentFile = config.sops.secrets.pve_env.path;

    exporters.snmp.enable = false; # FIXME
    exporters.snmp.configurationPath = builtins.path {
      name = "snmp.yaml";
      path = ../static/prometheus/snmp/snmp.yaml;
    };

    exporters.blackbox.enable = true;
    exporters.blackbox.configFile = ../static/prometheus/blackbox/blackbox.yaml;

    ruleFiles = [
      # rules from github.com/samber/awesome-prometheus-alerts
      ../static/prometheus/rules/embedded-exporter.yml
      ../static/prometheus/rules/node-exporter.yml
      ../static/prometheus/rules/blackbox-exporter.yml

      ../static/prometheus/rules/muccc.yaml
    ];

    alertmanagers = [
      {
        path_prefix = "/alertmanager";
        static_configs = [ { targets = [ "localhost:9093" ]; } ];
      }
    ];

    scrapeConfigs = [
      {
        job_name = "prometheus";
        static_configs = [ { targets = [ "localhost:9090" ]; } ];
      }
      {
        job_name = "alertmanager";
        metrics_path = "/alertmanager/metrics";
        static_configs = [ { targets = [ "localhost:9093" ]; } ];
      }
      {
        job_name = "node-exporter";
        static_configs = [ { targets = nodeExporterTargets; } ];
        relabel_configs = commonRelabelConfigs;
        metric_relabel_configs = [
          {
            source_labels = [
              "__name__"
              "name"
            ];
            separator = ";";
            regex = "^node_systemd_unit_state;(.*)\.(automount|device|mount|path|scope|slice|swap|target)$";
            action = "drop";
          }
        ];
      }
      {
        job_name = "proxmox";
        static_configs = [ { targets = [ "gustl.club.muc.ccc.de" ]; } ];
        metrics_path = "/pve";
        relabel_configs = [
          {
            source_labels = [ "__address__" ];
            target_label = "__param_target";
          }
          {
            source_labels = [ "__param_target" ];
            target_label = "instance";
          }
          {
            target_label = "__address__";
            replacement = "127.0.0.1:${toString config.services.prometheus.exporters.pve.port}";
          }
        ];
      }
      # {
      #   job_name = "snmp-exporter";
      #   static_configs = [ { targets = snmpTargets; } ];
      #   metrics_path = "/snmp";
      #   params = {
      #     auth = [ "public_v2" ];
      #     module = [ "if_mib" ];
      #   };
      #   relabel_configs = [
      #     {
      #       source_labels = [ "__address__" ];
      #       target_label = "__param_target";
      #     }
      #     {
      #       source_labels = [ "__param_target" ];
      #       target_label = "instance";
      #     }
      #     {
      #       target_label = "__address__";
      #       replacement = "127.0.0.1:${toString config.services.prometheus.exporters.snmp.port}";
      #     }
      #   ];
      # }
      {
        job_name = "knot-exporter";
        static_configs = [ { targets = knotTargets; } ];
        relabel_configs = commonRelabelConfigs;
      }
      {
        job_name = "mumble-exporter";
        scrape_interval = "2m";
        static_configs = [ { targets = [ "fasel.muc.ccc.de:64738" ]; } ];
        relabel_configs =
          [
            {
              source_labels = [ "__address__" ];
              target_label = "__param_host";
            }
            {
              source_labels = [ "__param_host" ];
              target_label = "instance";
            }
          ]
          ++ commonRelabelConfigs
          ++ [
            {
              target_label = "__address__";
              replacement = "127.0.0.1:8778";
            }
          ];
      }
      {
        job_name = "postfix-exporter";
        static_configs = [ { targets = postfixTargets; } ];
        relabel_configs = commonRelabelConfigs;
      }
      {
        job_name = "rspamd";
        scheme = "https";
        metrics_path = "/rspamd/metrics";
        basic_auth = {
          username = "briaftraeger";
          password = "briafgeheimnis";
        };
        static_configs = [ { targets = [ "briafzentrum.muc.ccc.de" ]; } ];
      }
      {
        job_name = "spaceapi";
        scheme = "https";
        static_configs = [ { targets = [ "api.muc.ccc.de" ]; } ];
      }
      {
        job_name = "authentik";
        scheme = "http";
        static_configs = [ { targets = [ "auth.club.muc.ccc.de:9300" ]; } ];
        relabel_configs = commonRelabelConfigs;
      }
      {
        job_name = "blackbox";
        static_configs = [ { targets = [ "localhost:9115" ]; } ];
      }
      {
        job_name = "blackbox-http";
        metrics_path = "/probe";
        params = {
          module = [ "http_2xx" ];
        };
        static_configs = [ { targets = httpNginxVhosts; } ];
        relabel_configs = [
          {
            source_labels = [ "__address__" ];
            target_label = "__param_target";
          }
          {
            source_labels = [ "__param_target" ];
            target_label = "instance";
          }
          {
            target_label = "__address__";
            replacement = "localhost:9115";
          }
        ];
      }
      {
        job_name = "nextcloud";
        static_configs = [ { targets = nextcloudTargets; } ];
        relabel_configs = commonRelabelConfigs;
      }
      {
        job_name = "postgres";
        static_configs = [ { targets = postgresTargets; } ];
        relabel_configs = commonRelabelConfigs;
      }
      {
        job_name = "gitea";
        scheme = "https";
        static_configs = [ { targets = giteaTargets; } ];
        relabel_configs = commonRelabelConfigs;
      }
      {
        job_name = "redis-exporter";
        static_configs = [ { targets = redisTargets; } ];
        relabel_configs = commonRelabelConfigs;
      }
      {
        job_name = "nginx";
        static_configs = [ { targets = nginxTargets; } ];
        relabel_configs = commonRelabelConfigs;
      }
    ];
  };

  services.prometheus-mumble-exporter.enable = true;

  services.prometheus.alertmanager = {
    enable = true;

    webExternalUrl = "https://prometheus.club.muc.ccc.de/alertmanager/";

    configuration = {
      route = {
        group_by = [
          "job"
          "instance"
          "severity"
        ];
        group_wait = "30s";
        group_interval = "5m";
        repeat_interval = "12h";
        receiver = "infra";
        routes = [
          {
            matchers = [
              "severity = critical"
              "instance = luftschleuse.club.muc.ccc.de"
            ];
            receiver = "muccc";
            repeat_interval = "1h";
          }
          {
            matchers = [ "severity = critical" ];
            receiver = "infra";
            repeat_interval = "1h";
          }
          {
            matchers = [ "severity = warning" ];
            receiver = "infra";
            repeat_interval = "6h";
          }
          {
            matchers = [ "severity = info" ];
            receiver = "devnull";
          }
        ];
      };

      inhibit_rules = [
        {
          source_matchers = [ "severity=critical" ];
          target_matchers = [ "severity=warning" ];
          equal = [
            "job"
            "instance"
            "severity"
          ];
        }
      ];

      receivers = [
        { name = "devnull"; }
        {
          name = "infra";
          webhook_configs = [
            {
              send_resolved = false;
              url = "http://localhost:8000/infra-muccc-alerts";
            }
            { url = "https://ntfy.bpletza.de/alertmanager/muccc"; }
          ];
        }
        {
          name = "muccc";
          webhook_configs = [ { url = "http://localhost:8000/muccc"; } ];
        }
      ];
    };
  };

  services.prometheus.alertmanagerIrcRelay = {
    enable = true;

    settings = {
      http_host = "localhost";
      http_port = 8000;

      irc_host = "irc.hackint.org";
      irc_port = 6697;
      irc_nickname = "alurm";

      irc_channels = [
        { name = "#infra-muccc-alerts"; }
        { name = "#muccc"; }
      ];

      msg_template = "Alert {{ .Labels.alertname }} ({{ .Labels.severity }}) on {{ .Labels.job }}/{{ .Labels.instance }} is {{ .Status }}";
    };
  };

  services.grafana = {
    enable = true;
    settings = {
      server = {
        http_addr = "[::1]";
        http_port = 3000;

        domain = "grafana.club.muc.ccc.de";
        root_url = "https://grafana.club.muc.ccc.de/";
      };

      "auth" = {
        "oauth_allow_insecure_email_lookup" = "true";
        "oauth_auto_login" = "true";
        "signout_redirect_url" = "https://auth.muc.ccc.de/application/o/grafana/end-session/";
      };

      "auth.generic_oauth" = {
        enabled = true;
        name = "authentik";
        client_id = "$__file{${config.sops.secrets.grafana-oidc-client.path}}";
        client_secret = "$__file{${config.sops.secrets.grafana-oidc-secret.path}}";
        auth_url = "https://auth.muc.ccc.de/application/o/authorize/";
        token_url = "https://auth.muc.ccc.de/application/o/token/";
        api_url = "https://auth.muc.ccc.de/application/o/userinfo/";
        scopes = "openid profile email";
        login_attribute_path = "preferred_username";
        allow_assign_grafana_admin = true;
        role_attribute_path = "grafana_role";
      };
    };
  };

  services.nginx = {
    enable = true;
    virtualHosts."prometheus.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      basicAuth.monitoring = "neigschaut";
      locations = {
        "/" = {
          proxyPass = "http://[::1]:9090/";
          extraConfig = ''
            satisfy any;
            allow 127.0.0.1;
            allow ::1;
            deny all;
          '';
        };
      };
    };

    virtualHosts."grafana.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations = {
        "/" = {
          proxyPass = "http://[::1]:3000";
        };
        "/alertmanager/" = {
          proxyPass = "http://[::1]:9093";
        };
      };
    };
  };
}
