{ config, pkgs, ... }:
let
  mucccZone = ../static/dns/muc.ccc.de.zone;
  clubZone = ../static/dns/club.muc.ccc.de.zone;
  badgeZone = ../static/dns/badge.events.ccc.de.zone;
  eh04Zone = ../static/dns/eh04.easterhegg.eu.zone;
  eh10Zone = ../static/dns/eh10.easterhegg.eu.zone;
  rev178Zone = ../static/dns/178.133.83.in-addr.arpa.zone;
  rev179Zone = ../static/dns/179.133.83.in-addr.arpa.zone;
  rev6Zone = ../static/dns/3.0.0.3.0.f.7.0.1.0.0.2.ip6.arpa.zone;
in
{
  system.stateVersion = "23.05";

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-label/boot";
      fsType = "vfat";
    };
  };

  swapDevices = [ { device = "/dev/disk/by-label/swap"; } ];

  networking = {
    hostName = "dns";
    domain = "club.muc.ccc.de";

    firewall.allowedTCPPorts = [
      53
      config.services.prometheus.exporters.knot.port
    ];
    firewall.allowedUDPPorts = [ 53 ];
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "76:a3:23:ad:57:e3";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [ "83.133.178.69/26" ];
      Gateway = [ "83.133.178.65" ];
      DNS = [ "83.133.178.65" ];
    };
  };

  services.knot = {
    enable = true;
    settingsFile = pkgs.writeText "knot.conf" ''
      server:
          listen: 83.133.178.69@53
          listen: 2001:7f0:3003:beef:74a3:23ff:fead:57e3@53
          automatic-acl: true
      remote:
        - id: zonk
          address: 194.126.158.124@53
      acl:
        - id: zonk_transfer
          address: [194.126.158.124/32, 2001:1578:400:23:fefe:d00f:23:42/128]
      template:
        - id: default
          dnssec-signing: off
          acl: [zonk_transfer]
          notify: zonk
          global-module: mod-stats
          # Input-only zone files
          # https://www.knot-dns.cz/docs/2.8/html/operation.html#example-3
          # prevents modification of the zonefiles, since the zonefiles are immutable
          zonefile-sync: -1
          zonefile-load: difference
          journal-content: changes
      mod-stats:
        - id: default
          request-protocol: on
          server-operation: on
          edns-presence: on
          flag-presence: on
          response-code: on
          reply-nodata: on
          query-type: on
      zone:
        - domain: muc.ccc.de
          file: ${mucccZone}
        - domain: muenchen.ccc.de
          file: ${mucccZone}
        - domain: club.muc.ccc.de
          file: ${clubZone}
        - domain: eh04.easterhegg.eu
          file: ${eh04Zone}
        - domain: eh10.easterhegg.eu
          file: ${eh10Zone}
        - domain: badge.events.ccc.de
          file: ${badgeZone}
        - domain: 178.133.83.in-addr.arpa
          file: ${rev178Zone}
        - domain: 179.133.83.in-addr.arpa
          file: ${rev179Zone}
        - domain: 3.0.0.3.0.f.7.0.1.0.0.2.ip6.arpa
          file: ${rev6Zone}
      log:
        - target: syslog
          any: info
    '';
  };

  services.prometheus.exporters.knot.enable = true;
}
