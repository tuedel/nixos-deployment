{
  system.stateVersion = "24.05";

  imports = [ ../profiles/actions-runner.nix ];

  sops = {
    secrets.gitea_registration_token = { };
  };

  networking = {
    firewall.allowedTCPPorts = [
      8093
      9090
    ];
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "DE:D3:39:B8:FC:74";
    linkConfig.Name = "upl0nk";
  };

  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [ "83.133.178.90/26" ];
      Gateway = [ "83.133.178.65" ];
    };
  };

  virtualisation.docker = {
    enable = true;
    autoPrune = {
      enable = true;
      dates = "daily";
    };
    listenOptions = [
      "/run/docker.sock"
      "127.0.0.1:2375"
    ];
  };

  muccc.nix-cache = {
    enable = true;
  };
}
