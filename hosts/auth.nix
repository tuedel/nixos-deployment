{ config, ... }:
{
  system.stateVersion = "23.05";

  sops = {
    defaultSopsFile = ../secrets/auth.yaml;
    secrets.authentik_secret = {
      restartUnits = [
        "authentik.service"
        "authentik-worker.service"
        "authentik-migrate.service"
      ];
    };
  };

  networking = {
    firewall.allowedTCPPorts = [ 9300 ];
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "ba:12:0c:2c:be:aa";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [ "83.133.178.77/26" ];
      Gateway = "83.133.178.65";
      DNS = [ "83.133.178.65" ];
    };
  };

  services.authentik = {
    enable = true;

    nginx = {
      enable = true;
      enableACME = true;
      host = "auth.muc.ccc.de";
    };

    environmentFile = config.sops.secrets.authentik_secret.path;
  };

  services.nginx = {
    enable = true;

    virtualHosts."auth.club.muc.ccc.de" = {
      locations."/".return = "301 https://auth.muc.ccc.de$request_uri";
    };
  };
}
