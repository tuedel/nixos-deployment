{
  config,
  inputs,
  pkgs,
  lib,
  ...
}:
{
  system.stateVersion = "23.05";

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "84:7b:eb:eb:20:dd";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = "yes";
      LLMNR = false;
    };
    dhcpV4Config = {
      ClientIdentifier = "mac";
    };
  };

  environment.systemPackages = [ pkgs.picocom ];

  services.klipper = {
    enable = true;
    firmwares.mcu = {
      enable = true;
      enableKlipperFlash = true;
      serial = "/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0";
      configFile = ../static/drugga/klipper-build-config;
    };
    configFile = ../static/drugga/printer-anycubic-i3-mega-2017.cfg;
  };

  services.moonraker = {
    enable = true;
    address = "::";
    settings = {
      authorization = {
        force_logins = false;
        cors_domains = [
          "localhost"
          "localhost:7125"
          "drugga.club.muc.ccc.de:7125"
          "drugga.club.muc.ccc.de"
        ];
        trusted_clients = [ "2001:a61:3ab3:f00:0:0:0:0/64" ];
      };
    };
  };

  systemd.services.moonraker = {
    serviceConfig.SupplementaryGroups = "klipper";
    restartTriggers = [ config.environment.etc."moonraker.cfg".source ];
  };

  services.nginx.upstreams.fluidd-apiserver.servers = lib.mkForce { "[::]:7125" = { }; };

  services.nginx.virtualHosts."drugga.club.muc.ccc.de" = {
    locations."/".proxyPass = "http://localhost:5000";
  };

  services.octoprint.enable = true;

  networking.firewall.allowedTCPPorts = [
    80
    443
    7125
  ];

  services.fluidd = {
    enable = true;
  };

  boot.loader.systemd-boot.enable = true;

  fileSystems."/" = lib.mkIf config.muccc.targetDeploy (
    lib.mkForce {
      fsType = "tmpfs";
      device = "tmpfs";
      options = [ "mode=0755" ];
    }
  );

  system.extraSystemBuilderCmds = ''
    ln -s ${config.system.build.diskoScript} $out/disko
  '';

  users.users.root = inputs.self.lib.humanoids.authorizedKeysMixinForUser "linus";

  boot.initrd.availableKernelModules = [
    "sdhci-pci"
    "mmc_block"
  ];

  services.fail2ban.enable = false;

  environment.persistence."/local" = {
    files = [
      "/etc/machine-id"
      "/etc/ssh/ssh_host_ed25519_key"
      "/etc/ssh/ssh_host_rsa_key"
      "/root/.bash_history"
    ];
    directories = [ "/var/lib/octoprint" ];
  };

  fileSystems."/local".neededForBoot = true;
  disko.devices.disk.mmcblk0 = {
    device = "/dev/mmcblk0";
    type = "disk";
    content = {
      type = "table";
      format = "gpt";
      partitions = [
        {
          name = "ESP";
          start = "1MiB";
          end = "256MiB";
          fs-type = "fat32";
          bootable = true;
          content = {
            type = "filesystem";
            format = "vfat";
            mountpoint = "/boot";
          };
        }
        {
          name = "persist";
          start = "256MiB";
          end = "100%";
          content = {
            type = "btrfs";
            mountOptions = [
              "compress=zstd"
              "noatime"
              "discard=async"
            ];
            mountpoint = "/persist";
            subvolumes = {
              "/nix" = {
                mountpoint = "/nix";
                mountOptions = [
                  "compress=zstd"
                  "noatime"
                  "discard=async"
                ];
              };
              "/local" = {
                mountpoint = "/local";
                mountOptions = [
                  "compress=zstd"
                  "noatime"
                  "discard=async"
                ];
              };
            };
          };
        }
      ];
    };
  };
}
