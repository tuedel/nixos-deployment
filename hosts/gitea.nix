{
  config,
  pkgs,
  lib,
  ...
}:
let
  catppuccinThemes = pkgs.stdenvNoCC.mkDerivation rec {
    pname = "catppuccin-gitea";
    version = "0.4.1";

    src = pkgs.fetchurl {
      url = "https://github.com/catppuccin/gitea/releases/download/v${version}/catppuccin-gitea.tar.gz";
      hash = "sha256-/P4fLvswitlfeaKaUykrEKvjbNpw5Q/nzGQ/GZaLyUI=";
    };

    sourceRoot = ".";

    postInstall = "
      install -vDt $out *
    ";
  };

  dimensions = {
    palette = [
      "Frappe"
      "Latte"
      "Macchiato"
      "Mocha"
    ];
    color = [
      "Blue"
      "Flamingo"
      "Green"
      "Lavender"
      "Maroon"
      "Mauve"
      "Peach"
      "Pink"
      "Red"
      "Rosewater"
      "Sapphire"
      "Sky"
      "Teal"
      "Yellow"
    ];
  };
  product = lib.attrsets.cartesianProductOfSets dimensions;
  variantName =
    { palette, color }:
    lib.concatStringsSep "-" [
      "catppuccin"
      (lib.strings.toLower palette)
      (lib.strings.toLower color)
    ];
  variants = map variantName product;
  catppuccinThemesList = lib.concatStringsSep "," variants;
in
{
  system.stateVersion = "23.05";

  sops = {
    defaultSopsFile = ../secrets/gitea.yaml;
    secrets.smtp_auth = {
      owner = config.services.gitea.user;
    };
  };

  networking = {
    hostName = "gitea";
    domain = "club.muc.ccc.de";
    firewall.allowedTCPPorts = [ config.services.prometheus.exporters.redis.port ];
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "92:dd:4f:7b:e3:eb";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [ "83.133.178.70/26" ];
      Gateway = [ "83.133.178.65" ];
    };
  };

  services.gitea = {
    enable = true;
    appName = "muCCC git";
    database = {
      type = "postgres";
    };
    mailerPasswordFile = config.sops.secrets.smtp_auth.path;
    lfs.enable = true;
    settings = {
      server = {
        ROOT_URL = "https://gitea.muc.ccc.de/";
        HTTP_ADDR = "::1";
        HTTP_PORT = 3022;
        DOMAIN = "gitea.muc.ccc.de";
        LANDING_PAGE = "explore";
      };
      repository = {
        DISABLE_STARS = true;
      };
      service = {
        DISABLE_REGISTRATION = true;
        SHOW_MILESTONES_DASHBOARD_PAGE = false;
        ENABLE_NOTIFY_MAIL = true;
      };
      "service.explore" = {
        DISABLE_USERS_PAGE = true;
      };
      session = {
        COOKIE_SECURE = true;
      };
      ui = {
        DEFAULT_THEME = "arc-green";
        SHOW_USER_EMAIL = false;
        THEMES = "auto,gitea,arc-green," + catppuccinThemesList;
      };
      metrics = {
        ENABLED = true;
      };
      actions = {
        ENABLED = true;
      };
      other = {
        SHOW_FOOTER_VERSION = false;
        SHOW_FOOTER_TEMPLATE_LOAD_TIME = false;
      };
      oauth2_client = {
        ENABLE_OPENID_SIGNUP = true;
        ENABLE_AUTO_REGISTRATION = true;
        REGISTER_EMAIL_CONFIRM = false;
        OPENID_CONNECT_SCOPES = "email profile gitea";
      };
      mailer = {
        ENABLED = true;
        PROTOCOL = "smtp+starttls";
        SMTP_ADDR = "briafzentrum.muc.ccc.de";
        SMTP_PORT = 25;
        USER = "gitea";
        FROM = "gitea@muc.ccc.de";
      };
      cache = {
        ENABLED = true;
        ADAPTER = "redis";
        HOST = "network=unix,addr=/run/redis-gitea/redis.sock,db=0";
      };
      indexer = {
        REPO_INDEXER_ENABLED = true;
      };

      packages.CHUNKED_UPLOAD_PATH = "${config.services.gitea.stateDir}/tmp/package-upload";
    };
  };

  services.redis.servers.gitea = {
    enable = true;
    inherit (config.services.gitea) user;
    port = 6379;
  };

  systemd.services.gitea = {
    preStart = ''
      # link catpuccin themes
      mkdir -p ${config.services.gitea.stateDir}/custom/public/css
      ${pkgs.coreutils}/bin/ln -sf ${catppuccinThemes}/* ${config.services.gitea.stateDir}/custom/public/css/
    '';
  };

  services.prometheus.exporters.redis.enable = true;

  services.nginx = {
    enable = true;
    virtualHosts."gitea.club.muc.ccc.de" = {
      serverAliases = [ "gitea.muc.ccc.de" ];
      enableACME = true;
      forceSSL = true;
      locations = {
        "/" = {
          proxyPass = "http://localhost:3022";
          extraConfig = ''
            client_max_body_size 2G;
          '';
        };
      };
    };
  };
}
