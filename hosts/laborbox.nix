{
  pkgs,
  lib,
  modulesPath,
  ...
}:

{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];
  deployment.targetHost = "laborbox.club.muc.ccc.de";
  deployment.targetUser = "root";

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "laborbox";

  time.timeZone = "Europe/Berlin";

  networking.useDHCP = false;
  networking.networkmanager = {
    enable = true;
    plugins = lib.mkForce [ ];
  };

  i18n.defaultLocale = "en_US.UTF-8";

  # Enable NFS. This is used to share directories between the host OS and the
  # Windows VM.
  services.nfs.server = {
    enable = true;
    exports = ''
      /home/muccc/export 192.168.122.0/24(rw,no_subtree_check,sync,no_root_squash,all_squash,anonuid=1001,anongid=100)
    '';
    lockdPort = 4001;
    mountdPort = 4002;
    statdPort = 4000;
    extraNfsdConfig = '''';
  };

  services.xserver = {
    enable = true;
    desktopManager = {
      xterm.enable = false;
      xfce.enable = true;
    };
    displayManager.defaultSession = "xfce";
  };

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/40e99392-d8e2-4a0d-80be-a5f59f28bdf4";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/FC2E-D0B3";
    fsType = "vfat";
  };

  virtualisation.libvirtd.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  users.users.muccc = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "libvirtd"
    ];
    password = "fnord42";
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG599UildOrAq+LIOQjKqtGMwjgjIxozI1jtQQRKHtCP q3k@mimeomia"
    ];
  };

  environment.systemPackages = with pkgs; [
    vim
    wget
    curl
    htop
    dstat
    usbutils
    spice-gtk
    firefox
    virt-manager
    inkscape
    solvespace
  ];

  # Enables USB redirection for libvirt (I think)
  # https://github.com/NixOS/nixpkgs/issues/106594
  virtualisation.spiceUSBRedirection.enable = true;

  networking.firewall.allowedTCPPorts = [
    # NFS
    111
    2049
    4000
    4001
    4002
    20048
  ];
  networking.firewall.allowedUDPPorts = [
    # NFS
    111
    2049
    4000
    4001
    4002
    20048
  ];
  # Todo?
  networking.firewall.enable = false;

  system.stateVersion = "21.11";
}
