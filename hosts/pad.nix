{ config, ... }:
{
  system.stateVersion = "23.05";

  sops = {
    defaultSopsFile = ../secrets/pad.yaml;
    secrets.hedgedoc_env = {
      owner = "hedgedoc";
    };
  };

  networking = {
    hostName = "pad";
    domain = "club.muc.ccc.de";
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "6E:23:5D:5E:DF:0B";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [ "83.133.178.100/26" ];
      Gateway = [ "83.133.178.65" ];
    };
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "hedgedoc" ];
    ensureUsers = [
      {
        name = "hedgedoc";
        ensureDBOwnership = true;
      }
    ];
  };

  services.hedgedoc = {
    enable = true;
    settings = {
      allowFreeURL = true;
      allowAnonymous = true;
      allowAnonymousEdits = true;
      allowPDFExport = true;
      allowEmailRegister = false;
      email = false;
      domain = "pad.muc.ccc.de";
      allowOrigin = [ "pad.muc.ccc.de" ];
      protocolUseSSL = true;
      db = {
        dialect = "postgres";
        host = "/run/postgresql";
        database = "hedgedoc";
      };
    };
    environmentFile = config.sops.secrets.hedgedoc_env.path;
  };

  services.nginx = {
    enable = true;
    virtualHosts."pad.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations = {
        "/" = {
          proxyPass = "http://localhost:3000";
        };

        "/socket.io/" = {
          proxyPass = "http://localhost:3000";
          proxyWebsockets = true;
        };
      };
    };
  };
}
