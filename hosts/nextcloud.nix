{
  config,
  pkgs,
  lib,
  ...
}:
{
  system.stateVersion = "23.05";

  sops = {
    defaultSopsFile = ../secrets/nextcloud.yaml;
    secrets.nextcloud_admin_pass = {
      owner = "nextcloud";
    };

    secrets.nextcloud_exporter_pass = {
      owner = "nextcloud-exporter";
    };
  };

  fileSystems = {
    "/var/lib/nextcloud" = {
      device = "/dev/disk/by-label/nextcloud-data";
    };
  };

  networking = {
    hostName = "nextcloud";
    domain = "club.muc.ccc.de";

    firewall.allowedTCPPorts = [ config.services.prometheus.exporters.redis.port ];
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "5E:48:49:E4:B1:B1";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [
        "2001:7f0:3003:beef:5c48:49ff:fee4:b1b1/64"
        "83.133.178.120/26"
      ];
      Gateway = [
        "2001:7f0:3003:beef::65"
        "83.133.178.65"
      ];
      DNS = [ "83.133.178.65" ];
    };
  };

  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud29;

    hostName = "nextcloud.club.muc.ccc.de";
    https = true;

    database.createLocally = true;
    configureRedis = true;

    config = {
      dbtype = "pgsql";
      adminpassFile = config.sops.secrets.nextcloud_admin_pass.path;
    };

    settings = {
      hide_login_form = true;
      user_oidc = {
        allow_multiple_user_backends = 0;
      };
      default_phone_region = "DE";
    };

    phpOptions = lib.mkOptionDefault {
      "opcache.interned_strings_buffer" = lib.mkForce "32";
      "opcache.max_accelerated_files" = lib.mkForce "100000";
      "opcache.memory_consumption" = lib.mkForce "256";
      "opcache.jit" = "1255";
      "opcache.jit_buffer_size" = "128M";
    };

    extraApps = {
      inherit (config.services.nextcloud.package.packages.apps) calendar polls groupfolders;

      user_oidc = pkgs.fetchNextcloudApp {
        url = "https://github.com/nextcloud-releases/user_oidc/releases/download/v5.0.2/user_oidc-v5.0.2.tar.gz";
        sha256 = "sha256-8r0pb8pt/rkrqz/0bqqWBkJsaaPrNtnnhdB0iZgJh1o=";
        license = "free";
      };

      calendar_resource_management = pkgs.fetchNextcloudApp {
        url = "https://github.com/nextcloud-releases/calendar_resource_management/releases/download/v0.7.0/calendar_resource_management-v0.7.0.tar.gz";
        sha256 = "sha256-uLOyW1bd15NRalps7/7bzA1QC/RQQYw+4mkwlI3poTc=";
        license = "free";
      };

      richdocuments = pkgs.fetchNextcloudApp {
        url = "https://github.com/nextcloud-releases/richdocuments/releases/download/v8.4.2/richdocuments-v8.4.2.tar.gz";
        sha256 = "sha256-pmMww0qba+SKmpNYf2ZmNWZNn2IjJ7HkO09Xf9bzb70=";
        license = "free";
      };
    };

    extraAppsEnable = true;
  };

  services.nginx.virtualHosts = {
    "nextcloud.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
    };
    "nextcloud-office.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations = {
        # static files
        "^~ /browser" = {
          proxyPass = "http://localhost:9980";
        };
        # WOPI discovery URL
        "^~ /hosting/discovery" = {
          proxyPass = "http://localhost:9980";
        };

        # Capabilities
        "^~ /hosting/capabilities" = {
          proxyPass = "http://localhost:9980";
        };

        # download, presentation, image upload and websocket
        "~ ^/cool" = {
          proxyPass = "http://localhost:9980";
          proxyWebsockets = true;
        };

        # Admin Console websocket
        "^~ /cool/adminws" = {
          proxyPass = "http://localhost:9980";
          proxyWebsockets = true;
          extraConfig = ''
            proxy_read_timeout 36000s;
          '';
        };
      };
    };
  };

  virtualisation.oci-containers = {
    backend = "docker";
    containers.collabora = {
      image = "collabora/code";
      imageFile = pkgs.dockerTools.pullImage {
        imageName = "collabora/code";
        imageDigest = "sha256:fc8d72dc8c2cc6750b3ce28ebd42b12d6d1b66f76d1669ad7f64b51dd380a791";
        sha256 = "sha256-mVGF077J22NhpoSP2zw/+e2v1foX8dXXP38lCqAHGZk=";
      };
      ports = [ "9980:9980" ];
      environment = {
        domain = "nextcloud.club.muc.ccc.de";
        extra_params = "--o:ssl.enable=false --o:ssl.termination=true";
      };
      extraOptions = [
        "--cap-add"
        "MKNOD"
      ];
    };
  };

  services.prometheus.exporters.redis.enable = true;
  services.prometheus.exporters.nextcloud = {
    enable = true;
    openFirewall = true;

    url = "https://nextcloud.club.muc.ccc.de";
    username = "root";
    passwordFile = config.sops.secrets.nextcloud_exporter_pass.path;
  };
}
