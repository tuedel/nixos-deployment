{ inputs, ... }:
{
  perSystem =
    {
      pkgs,
      system,
      lib,
      self',
      ...
    }:
    {
      packages = {
        a2dp-agent = pkgs.callPackage ./pkgs/a2dp-agent.nix { };

        inherit (pkgs) attic-client skopeo;
        nix-fast-build = inputs.nix-fast-build.packages.${system}.default;

        sopsYaml = pkgs.callPackage ./pkgs/sops.yaml.nix { inherit (inputs) self; };
        repl = pkgs.writers.writeBashBin "repl" (lib.readFile ./static/nixos-repl.sh);

        sops-updatekeys = pkgs.writers.writeBashBin "sops-updatekeys" ''
          for secretfn in secrets/*.yaml; do
            ${pkgs.sops}/bin/sops --config=${self'.packages.sopsYaml} updatekeys $secretfn
          done
        '';

        issue-after-flake-update = pkgs.callPackage ./pkgs/issue-after-flake-update.nix { };
      };

      legacyPackages = {
        giteaNixosConfigurations = pkgs.callPackage ./pkgs/gitea-nixos-configurations.nix {
          inherit (self'.legacyPackages) colmenaNixosConfigurations;
        };
      };
    };
}
