{
  name,
  inputs,
  config,
  pkgs,
  lib,
  ...
}:
let
  selfLib = inputs.self.lib;
  cfg = config.muccc;
in
{
  imports = [
    inputs.lix-module.nixosModules.default
    inputs.sops-nix.nixosModules.sops
    inputs.mumble-exporter.nixosModules.default
    inputs.home-manager.nixosModules.home-manager
    inputs.authentik.nixosModules.default
    inputs.disko.nixosModules.disko
    inputs.impermanence.nixosModules.impermanence
  ];

  options = {
    muccc = {
      enable = lib.mkOption {
        type = lib.types.bool;
        default = true;
        description = "muccc default module";
      };
      targetDeploy = lib.mkEnableOption "Whether we build the target configuration or just a testing VM";
    };
  };

  config = lib.mkIf cfg.enable {
    nixpkgs.overlays = [
      inputs.bad_gateway.overlays.default
      inputs.muccc-api.overlays.default
      inputs.luftschleuse2.overlays.default
    ];

    # We are mostly running x86_64-linux, overrides in hardware specific modules
    # Little more prio than mkDefault because this is set by eval-config for currentSystem
    nixpkgs.system = lib.mkOverride 999 "x86_64-linux";

    sops = {
      defaultSopsFile = ../secrets + "/${config.networking.hostName}.yaml";
    };

    networking = {
      hostName = lib.mkDefault name;
      domain = lib.mkDefault "club.muc.ccc.de";
      useDHCP = false;
      useNetworkd = true;
      firewall = {
        logRefusedConnections = false;
      };
    };

    time.timeZone = lib.mkDefault "UTC";
    services.getty.helpLine = lib.mkForce ''
      ip6: \6
      ip4: \4
    '';

    boot = {
      kernelPackages = lib.mkDefault pkgs.linuxPackages_latest;
      supportedFilesystems.zfs = lib.mkForce false;

      tmp = {
        useTmpfs = true;
        tmpfsSize = "100%";
      };

      loader = {
        timeout = lib.mkDefault 1;
        grub.splashImage = null;
      };

      enableContainers = false;
    };
    zramSwap = {
      enable = true;
      algorithm = "lz4";
      memoryPercent = 100;
    };

    environment.sessionVariables = {
      # omit S to wrap lines
      SYSTEMD_LESS = "FRXMK";
    };
    environment.defaultPackages = [ ];
    environment.stub-ld.enable = false;
    environment.systemPackages = with pkgs; [
      wget
      curl
      htop
      iftop
      tmux
      tcpdump
      rsync
      git
      lsof
      screen
      socat
      nmap
      ncdu
      iptables
      pciutils
      usbutils
      alacritty.terminfo
      foot.terminfo
      kitty.terminfo
      rxvt-unicode-unwrapped.terminfo
      jq
      strace
    ];

    programs = {
      bash.enableCompletion = true;
      vim.defaultEditor = true;
      zsh.enable = true;
      mtr.enable = true;
      command-not-found.enable = false;
    };

    documentation = {
      doc.enable = false;
      info.enable = false;
    };

    services.journald.extraConfig = ''
      SystemMaxUse=200M
      MaxRetentionSec=2d
    '';

    services.openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = lib.mkDefault false;
        UseDns = false;
      };
    };

    services.fail2ban = {
      enable = lib.mkDefault true;
      ignoreIP =
        # derchris nixos
        [ "194.233.171.14/24" ]
        # muccc prefixes
        ++ [
          "83.133.178.0/23"
          "2001:07F0:3003::/48"
        ]
        # club client prefix
        ++ lib.optionals (config.networking.domain == "club.muc.ccc.de") [ "192.168.178.0/23" ];

      jails = {
        sshd.settings = {
          enabled = config.services.openssh.enable;
          filter = "sshd[mode=aggressive]";
        };

        nginx-http-auth.settings = {
          enabled = config.services.nginx.enable;
          filter = "nginx-http-auth[mode=aggressive]";
          # shipped failregex doesnt work properly when reading from journald
          failregex = ''\s*\[error\] \d+#\d+: \*\d+ user "(?:[^"]+|.*?)":? (?:password mismatch|was not found in "[^\"]*"), client: <HOST>, server: \S*, request: "\S+ \S+ HTTP/\d+\.\d+", host: "\S+"(?:, referrer: "\S+")?\s*$'';
        };

        dovecot.settings = {
          enabled = config.services.dovecot2.enable;
          filter = "dovecot[mode=aggressive]";
        };

        postfix.settings = {
          enabled = config.services.postfix.enable;
          filter = "postfix[mode=aggressive]";
        };
      };
    };

    users = {
      mutableUsers = false;
      users =
        (selfLib.humanoids.mapUsersByGroup "infra-admins" (
          _: v: {
            inherit (v) openssh;
            isNormalUser = true;
            extraGroups = [
              "wheel"
              "systemd-journal"
              "docker"
            ];
          }
        ))
        // {
          root = {
            # secrets/credentials.yaml
            hashedPassword = "$y$j9T$Bj/I7HI489OaHV06xw/rJ.$AnFELq9yIKf96vaSuC2oE7m3Jsmu7yql/XFq7rPLC91";
            openssh.authorizedKeys.keys = lib.foldlAttrs (
              acc: _: v:
              acc ++ v.openssh.authorizedKeys.keys
            ) [ ] (selfLib.humanoids.getUsersByGroup "infra-admins");
          };
        };
    };

    # Configs for local development virtual machines
    formatConfigs =
      lib.genAttrs
        [
          "vm"
          "vm-nogui"
        ]
        (_: {
          services.getty.autologinUser = "root";
          users.users.root.hashedPassword = lib.mkForce "";
        });

    security.sudo = {
      execWheelOnly = lib.mkDefault true;
      wheelNeedsPassword = false;
    };

    system = {
      # show a diff of the system closure on activation
      activationScripts.diff = ''
        if [[ -e /run/current-system ]]; then
          ${pkgs.nix}/bin/nix store diff-closures /run/current-system "$systemConfig"
        fi
      '';

      # include git rev of this repo/flake into the nixos-version
      configurationRevision = if inputs.self ? rev then lib.substring 0 8 inputs.self.rev else "dirty";

      nixos = {
        revision = inputs.nixpkgs.rev;
        versionSuffix = lib.mkForce ".${inputs.nixpkgs.shortRev}-${config.system.configurationRevision}";
      };
    };

    # set nixpkgs on the target to the nixpkgs version of the deployment
    nix.registry.nixpkgs.flake = inputs.nixpkgs;
    nix.nixPath = [ "nixpkgs=/run/nixpkgs" ];
    systemd.tmpfiles.rules = [
      "L+ /run/nixpkgs - - - - ${inputs.nixpkgs}"
      "L+ /run/muccc-nixos-deployment - - - - ${inputs.self}"
    ];

    security.acme = {
      defaults.email = "noc@muc.ccc.de";
      acceptTerms = true;
    };

    nix.settings = {
      extra-experimental-features = [
        "flakes"
        "nix-command"
      ];
      connect-timeout = 5;
      http-connections = 50;
      trusted-users = [
        "root"
        "@wheel"
      ];
      substituters = [
        "https://cache.muc.ccc.de/muccc"
        "https://nix-community.cachix.org"
      ];
      trusted-substituters = [
        "https://cache.muc.ccc.de/muccc"
        "https://nix-community.cachix.org"
      ];
      trusted-public-keys = [
        "muccc:ATRH5jlmJyn4kpMmnhMVIbEugyr8lsTYvi5KmadQvZk="
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
    };

    nix.gc = {
      automatic = true;
      dates = "weekly";
      randomizedDelaySec = "1h";
      options = "--delete-older-than 7d";
    };

    services.prometheus.exporters = {
      node = {
        enable = true;
        enabledCollectors = [
          "systemd"
          "textfile"
          "textfile.directory /run/prometheus-node-exporter"
          (lib.replaceStrings [ "\\" ] [
            "\\\\"
          ] "systemd.unit-exclude='.+\\.(automount|device|scope|slice)'")
        ];
        port = 9100;
        openFirewall = true;
      };
      nginx = {
        enable = config.services.nginx.enable && config.services.nginx.statusPage;
        listenAddress = "[::]";
        openFirewall = true;
      };
    };

    sound.enable = lib.mkDefault false;

    xdg = {
      autostart.enable = lib.mkDefault false;
      icons.enable = lib.mkDefault false;
      mime.enable = lib.mkDefault false;
      sounds.enable = false;
    };
  };
}
