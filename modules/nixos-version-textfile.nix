{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
let
  metrics-file = "/run/prometheus-node-exporter/nixos-version.prom";
  prometheus-group = config.systemd.services.prometheus-node-exporter.serviceConfig.Group;
in
{
  options.services.prometheus-nixos-version.enable = lib.mkEnableOption "NixOS version metrics" // {
    default = true;
  };

  config = lib.mkIf config.services.prometheus-nixos-version.enable {
    systemd.tmpfiles.rules = [ "f ${metrics-file} 0660 root ${prometheus-group}" ];
    systemd.services.prometheus-nixos-version = {
      path = [
        pkgs.jq
        pkgs.nix
      ];
      wantedBy = [ "multi-user.target" ];
      script = ''
        #!${pkgs.runtimeShell}
        set -exuo pipefail
        export HOME=$PWD
        systemTimestamp=$(nix path-info --json /run/current-system | jq '.[0].registrationTime')
        nixpkgsTimestamp=${lib.escapeShellArg inputs.nixpkgs.sourceInfo.lastModified}
        rebootRequired=$(test $(readlink -f /run/current-system/kernel) == $(readlink -f /run/booted-system/kernel); echo $?)
        mkdir -p /run/prometheus-node-exporter
        /run/current-system/sw/bin/nixos-version --json |
          jq -rf ${../static/modules/nixos-version-textfile.jq} \
            --arg systemTimestamp "$systemTimestamp" \
            --arg nixpkgsTimestamp "$nixpkgsTimestamp" \
            --arg rebootRequired "$rebootRequired" \
             > ${metrics-file}
      '';
      restartTriggers = [
        config.system.nixos.version
        config.system.nixos.release
      ];
      serviceConfig = {
        DynamicUser = true;
        SupplementaryGroups = [ prometheus-group ];
        ReadWritePaths = [ metrics-file ];
        PrivateNetwork = true;
        ProtectHome = true;
        PrivateDevices = true;
        CapabilityBoundingSet = "";
        RestrictAddressFamilies = "AF_UNIX";
        RestrictNamespaces = true;
        IPAddressDeny = "any";
        ProtectClock = true;
        ProtectControlGroups = true;
      };
    };
  };
}
