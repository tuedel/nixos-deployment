{
  lib,
  config,
  pkgs,
  ...
}:
let
  cfg = config.muccc.nginx;
in
{
  options.muccc.nginx = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = true;
      description = "NGINX optimizations";
    };

    openFirewall = lib.mkOption {
      type = lib.types.bool;
      default = true;
    };
  };

  config = lib.mkIf cfg.enable {
    services.nginx = {
      package = pkgs.nginxMainline;
      recommendedOptimisation = true;
      recommendedTlsSettings = true;
      recommendedGzipSettings = true;
      recommendedBrotliSettings = true;
      recommendedProxySettings = true;
      clientMaxBodySize = "128m";

      # needs resolver for for ocsp stapling
      resolver.addresses = [
        (if config.services.resolved.enable then "127.0.0.53" else "194.150.168.168")
      ];

      logError = "stderr info";
      commonHttpConfig = ''
        access_log syslog:server=unix:/dev/log,facility=local4,severity=debug,nohostname;

        proxy_headers_hash_max_size 1024;
        proxy_headers_hash_bucket_size 256;
      '';

      # for monitoring
      statusPage = true;
    };

    networking.firewall = lib.mkIf cfg.openFirewall {
      allowedTCPPorts = [
        443
        80
      ];
      allowedUDPPorts = [ 443 ];
    };
  };
}
