{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.muccc.rpi;
in
{
  options = {
    muccc.rpi.enable = lib.mkEnableOption "RaspberryPi defaults";
  };

  config = lib.mkIf (config.muccc.targetDeploy && cfg.enable) {
    nixpkgs.system = "aarch64-linux";

    fileSystems = {
      "/boot/firmware" = {
        device = "/dev/disk/by-label/FIRMWARE";
        fsType = "vfat";
        options = [
          "nofail"
          "noauto"
        ];
      };
      "/" = {
        device = lib.mkDefault "/dev/disk/by-label/NIXOS_SD";
        fsType = "ext4";
        options = [ "noatime" ];
      };
    };

    boot = {
      initrd = {
        includeDefaultModules = false;
        availableKernelModules = [
          "mmc_block"
          "usbhid"
          "usb_storage"
          "vc4"
          "pcie_brcmstb" # required for the pcie bus to work
          "reset-raspberrypi" # required for vl805 firmware to load
        ];
      };
      supportedFilesystems = lib.mkForce [
        "vfat"
        "btrfs"
        "f2fs"
      ];
      kernelParams = [ "console=tty0" ];
      loader = {
        grub.enable = false;
        generic-extlinux-compatible.enable = true;
      };
    };

    hardware = {
      enableRedistributableFirmware = true;
      deviceTree.filter = lib.mkDefault "bcm*-rpi-*.dtb";
    };

    networking = {
      wireless = {
        enable = false; # for bluetooth audio
        networks = {
          "muccc.legacy-5GHz".psk = "haileris";
          "muccc.legacy-2.4GHz".psk = "haileris";
        };
        interfaces = [ "wlan" ];
      };
      firewall = {
        trustedInterfaces = [
          "upl0nk"
          "wlan"
        ];
      };
    };

    environment.systemPackages = with pkgs; [
      lm_sensors
      libraspberrypi
      raspberrypi-eeprom
    ];

    # hack to get some opengl features working on panfrost gpus
    environment.sessionVariables.PAN_MESA_DEBUG = "gl3";

    services.resolved = {
      dnssec = "false"; # pi loses power when hq is down, time is not reliable
    };
  };
}
