{
  config,
  inputs,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.muccc.multimedia;
in
{
  options = {
    muccc.multimedia = {
      enable = lib.mkEnableOption "multimediale unterhaltung";
      username = lib.mkOption { type = lib.types.str; };
      autostartTmuxp = lib.mkOption {
        type = lib.types.bool;
        default = false;
      };
      greetdLoginCmd = lib.mkOption {
        type = lib.types.str;
        default = "sway";
      };
    };
  };

  config = lib.mkIf cfg.enable {
    networking.firewall.allowedTCPPorts = [
      6680 # mpd
    ];

    services.openssh.settings.PasswordAuthentication = true;

    sound.enable = true;
    xdg = {
      icons.enable = true;
      mime.enable = true;
    };

    hardware.bluetooth = {
      enable = true;
      powerOnBoot = true;
      settings = {
        General = {
          MultiProfile = "multiple";
          FastConnectable = true;
        };
      };
    };

    services.nginx = {
      enable = true;
      virtualHosts."hauptraumpi.club.muc.ccc.de" = {
        locations."/".proxyPass = "http://localhost:8081";
      };
    };

    services.mympd = {
      enable = true;
      settings = {
        http_port = 8081;
      };
    };
    users = {
      users.${cfg.username} = {
        isNormalUser = true;
        password = "alarm";
        extraGroups = [
          "wheel"
          "audio"
          "video"
          "dialout"
        ];
        linger = true;
        openssh.authorizedKeys.keys =
          config.users.users.root.openssh.authorizedKeys.keys
          ++ inputs.self.lib.humanoids.getSSHKeysForUsers [
            "fpletz"
            "derandere"
            "tuedel"
            "sebtm"
          ]
          ++ [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHHvo5N+o0polCh5Yqbg7TOVtWSte6uyyDEk/9IG6AW3 v@x230" ];
      };
    };

    home-manager.users.${cfg.username} =
      { pkgs, ... }:
      {
        home.stateVersion = "24.05";
        systemd.user.startServices = "sd-switch";

        nix = {
          gc = {
            automatic = true;
            options = "-d";
            frequency = "daily";
          };
        };

        gtk = {
          enable = true;
          theme = {
            name = "Arc-Dark";
            package = pkgs.arc-theme;
          };
          iconTheme = {
            name = "Papirus-Dark";
            package = pkgs.papirus-icon-theme;
          };
          gtk3.extraConfig = {
            gtk-application-prefer-dark-theme = true;
          };
          gtk4.extraConfig = {
            gtk-application-prefer-dark-theme = true;
          };
        };

        qt = {
          enable = true;
          platformTheme.name = "gtk";
        };

        programs.bash.enable = true;

        programs.tmux = {
          enable = true;
          aggressiveResize = true;
          clock24 = true;
          terminal = "tmux-256color";
          plugins = [
            {
              plugin = pkgs.tmuxPlugins.resurrect;
              extraConfig = ''
                set -g @resurrect-processes 'ssh cmatrix "~mpv->mpv *" "~bash->bash *" "~vis->vis" "~btop->btop" htop "~python3->python3 *" mmtc bluetoothctl gomatrix "~unimatrix->unimatrix *" wttr'
                set -g @resurrect-dir '~/.local/state/tmux/resurrect'
              '';
            }
            {
              plugin = pkgs.tmuxPlugins.continuum;
              extraConfig = ''
                set -g @continuum-restore 'on'
                set -g @continuum-save-interval '1' # minutes
              '';
            }
          ];
        };

        programs.foot = {
          enable = true;
          server.enable = true;
          settings = {
            main = {
              font = "Fira Code:size=12";
            };
            colors = {
              alpha = 0.95;
            };
          };
        };

        services.mpd = {
          enable = true;
          musicDirectory = "/home/${cfg.username}";
          network.listenAddress = "any";
          extraConfig = ''
            audio_output {
              type "pipewire"
              name "pipewire"
            }
            zeroconf_enabled "yes"
            zeroconf_name "mpd @ %h"
          '';
        };

        systemd.user.services.mpd = {
          Unit.BindsTo = [ "pipewire.service" ];
          Service.ExecStartPost = "${lib.getExe pkgs.mpc-cli} play";
        };

        wayland.windowManager.sway = {
          enable = true;
          wrapperFeatures.gtk = true;
          config = {
            modifier = "Mod4";
            bars = [ ];
            terminal = "foot";
            window = {
              titlebar = false;
              hideEdgeBorders = "smart";
            };
            floating = {
              titlebar = false;
            };
            startup = [ { command = "${pkgs.foot}/bin/foot tmux attach"; } ];
            output = {
              HDMI-A-1.bg = "${./../static/multimedia/wau.jpg} fill";
              HEADLESS-1 = {
                mode = "1920x1080@60Hz";
              };
            };
          };
        };

        programs.waybar = {
          enable = true;
          systemd.enable = true;
        };

        programs.mpv = {
          enable = true;
          config = {
            vo = "gpu";
            gpu-context = "wayland";
            ytdl-format = "bestvideo[ext=webm][height<=?1080]+bestaudio[acodec=opus]/bestvideo[height<=?1080]+bestaudio/best";
          };
        };

        home.packages = with pkgs; [
          yt-dlp
          pulsemixer
          cmatrix
          screen
          cli-visualizer
          playerctl
          mpc-cli
          mmtc
          btop
          gomatrix
          unimatrix
          (writers.writeBashBin "wttr" ''
            while true; do
              clear
              ${lib.getExe curl} -s wttr.in/~muccc?1 | head -n17 | tail -n10 && sleep 175
              sleep 5
            done
          '')
        ];

        services.spotifyd = {
          enable = true;
          package = pkgs.spotifyd.override {
            withALSA = false;
            withPortAudio = false;
            withMpris = true;
          };
          settings.global = {
            device_name = config.networking.hostName;
            backend = "pulseaudio";
            bitrate = 320;
            device_type = "computer";
          };
        };

        programs.librewolf = {
          enable = true;
          package = pkgs.librewolf-wayland.override (_attrs: {
            extraPrefs = ''
              pref("ui.systemUsesDarkTheme", 1);
              pref("privacy.webrtc.legacyGlobalIndicator", false);
              pref("media.ffmpeg.vaapi.enabled", true);
              pref("gfx.webrender.enabled", true);
              pref("gfx.webrender.all", true);
              pref("gfx.webrender.compositor", true);
              pref("gfx.webrender.compositor.force-enabled", true);
              pref("webgl.disabled", false);
              pref("privacy.resistFingerprinting", false);
              pref("network.dns.echconfig.enabled", true);
              pref("network.dns.http3_echconfig.enabled", true);
              pref("geo.enabled", false);
            '';
          });
        };

        programs.chromium = {
          enable = true;
          package = pkgs.ungoogled-chromium;
          commandLineArgs = [
            "--enable-features=WebRTCPipeWireCapturer,VaapiVideoDecoder"
            "--ignore-gpu-blocklist"
            "--enable-gpu-rasterization"
            "--disable-background-networking"
            "--enable-accelerated-video-decode"
            "--enable-zero-copy"
            "--no-default-browser-check"
            "--disable-sync"
            "--disable-features=MediaRouter"
            "--enable-features=UseOzonePlatform"
          ];
          extensions = [
            "cjpalhdlnbpafiamejdnhcphjbkeiagm" # ublock origin
            "pgdnlhfefecpicbbihgmbmffkjpaplco" # ublock extra
          ];
        };

        systemd.user.services.shairport-sync = {
          Unit = {
            After = [
              "network.target"
              "sound.target"
              "avahi-daemon.service"
            ];
            Description = "Shairport-Sync - Apple Airplay";
          };

          Install.WantedBy = [ "default.target" ];

          Service = {
            ExecStart = "${pkgs.shairport-sync}/bin/shairport-sync -v -o pa";
            RuntimeDirectory = "shairport-sync";
            Restart = "always";
          };
        };

        systemd.user.services.bt-agent-a2dp = {
          Unit = {
            Description = "A2DP Bluetooth Agent";
          };
          Install.WantedBy = [ "default.target" ];
          Service = {
            ExecStartPre = "${pkgs.bluez}/bin/bluetoothctl discoverable on";
            ExecStart = lib.getExe inputs.self.packages.${pkgs.system}.a2dp-agent;
            Environment = "PYTHONUNBUFFERED=1";
          };
        };
      };

    services.pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
    };
    security.rtkit.enable = true;

    programs.sway.enable = true;
    fonts.packages = with pkgs; [
      fira
      fira-code
      fira-code-symbols
      fira-mono
      powerline-fonts
      font-awesome_4
      font-awesome_5
    ];

    services.greetd = {
      enable = true;
      settings = {
        initial_session = {
          command = cfg.greetdLoginCmd;
          user = cfg.username;
        };
        default_session = {
          command = "${
            lib.makeBinPath [ pkgs.greetd.tuigreet ]
          }/tuigreet --time --cmd '${cfg.greetdLoginCmd}'";
          user = "greeter";
        };
      };
    };

    services.resolved = {
      llmnr = "false"; # we use avahi instead
    };
    services.avahi = {
      enable = true;
      publish = {
        enable = true;
        addresses = true;
        domain = true;
        hinfo = true;
        userServices = true;
      };
    };
  };
}
