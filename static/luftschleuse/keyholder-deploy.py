import http.client
import json
import os
import os.path
import sys
import tarfile

REPO = "muccc/keyholder"
TOKEN = os.environ.get("GITEA_TOKEN")
STATE_DIRECTORY = os.environ.get("STATE_DIRECTORY", "/tmp")


def req(*args, headers={}, **kwargs):
    conn = http.client.HTTPSConnection("gitea.muc.ccc.de")
    headers.update({"Authorization": f"token {TOKEN}"})
    conn.request(*args, headers=headers, **kwargs)
    return conn.getresponse()


def req_json(*args, **kwargs):
    return json.load(req(*args, **kwargs))


def set_status(status):
    statuses = req_json(
        "POST",
        f"/api/v1/repos/{REPO}/statuses/{head_commit['sha']}",
        body=json.dumps(
            {
                "context": "luftschleuse / deploy",
                "state": status,
            }
        ),
        headers={"Content-Type": "application/json"},
    )
    if "id" not in statuses:
        sys.exit(1)


if __name__ == "__main__":
    head_commit = req_json("GET", f"/api/v1/repos/{REPO}/commits?limit=1")[0]
    statuses = req_json(
        "GET",
        f"/api/v1/repos/{REPO}/statuses/{head_commit['sha']}",
    )

    already_deployed = False
    for status in statuses:
        if (
            status["status"] == "success"
            and status["context"] == "luftschleuse / deploy"
        ):
            already_deployed = True
            break

    if already_deployed:
        sys.exit(42)

    set_status("pending")

    src_fn = os.path.join(STATE_DIRECTORY, "authorized_keys.new")
    with open(
        src_fn,
        "w",
    ) as outfile:
        with tarfile.open(
            mode="r|gz",
            fileobj=req(
                "GET",
                f"/api/v1/repos/{REPO}/archive/{head_commit['sha']}.tar.gz",
            ),
        ) as tar:
            for f in tar:
                if f.name.startswith("keyholder/keys/"):
                    outfile.write(f"# {f.name}\n")
                    outfile.write(tar.extractfile(f).read().decode())
                    outfile.write("\n")

    os.rename(
        src_fn,
        os.path.join(STATE_DIRECTORY, "authorized_keys"),
    )

    set_status("success")
