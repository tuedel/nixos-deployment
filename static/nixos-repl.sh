#!/usr/bin/env bash
flake="${FLAKE_ROOT:-.}"
flakeAttr="${1:-nixbus}"
d='$'
q='"'
bold="$(echo -e '\033[1m')"
blue="$(echo -e '\033[34;1m')"
attention="$(echo -e '\033[35;1m')"
reset="$(echo -e '\033[0m')"
if [[ -e $flake ]]; then
  flakePath=$(realpath "$flake")
else
  flakePath=$flake
fi
exec nix repl --expr "
  let flake = builtins.getFlake ''$flakePath'';
      configuration = flake.legacyPackages.\${builtins.currentSystem}.colmenaNixosConfigurations.$flakeAttr;

      motd = ''
        $d{$q\n$q}
        Loaded
            ${attention}$flakeAttr${reset}
            in ${bold}$flake${reset}

        The following is loaded into nix repl's scope:

            - ${blue}config${reset}   All option values
            - ${blue}options${reset}  Option data and metadata
            - ${blue}pkgs${reset}     Nixpkgs package set
            - ${blue}lib${reset}      Nixpkgs library functions
            - other module arguments
            - ${blue}flake${reset}    Flake outputs, inputs and source info of $flake

        Use tab completion to browse around ${blue}config${reset}.
        Use ${bold}:r${reset} to ${bold}reload${reset} everything after making a change in the flake.
        See ${bold}:?${reset} for more repl commands.
      '';
      scope =
        assert configuration._type or null == ''configuration'';
        assert configuration.class or ''nixos'' == ''nixos'';
        configuration._module.args //
        configuration._module.specialArgs //
        {
          inherit (configuration) config options;
          lib = configuration.lib or configuration.pkgs.lib;
          inherit flake;
        };
  in builtins.seq scope builtins.trace motd scope
"
